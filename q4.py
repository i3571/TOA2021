def read_data(filepath):
    contents = {}

    with open(filepath, 'r') as f:
        for line in f:
            code, price, name = line.strip('\n').split(', ')
            contents[code] = [float(price), name]

    return contents


def save_data(prod_dict, filepath):

    with open(filepath, 'w') as f:
        first_line = True
        for code, (price, name) in sorted(prod_dict.items(), key=lambda x: x[1]):
            format = f'{code}, {price}, {name}'
            if first_line:
                f.write(f'{format}')
                first_line = False
            else:
                f.write(f'\n{format}')


def main():

    file = './products.txt'
    product_dict = read_data(file)

    while True:

        print(product_dict)

        product_code = input('Enter code of product: ') or ''

        if product_code == '':
            save_data(product_dict, file)
            print('Program ends')
            break

        product_price = float(input('Enter price of product: '))

        if product_price <= 0:
            print('Price must be a positive value')
            continue

        if product_code in product_dict:
            print(f'Updating entry {product_code} with new price {product_price}')
            product_dict[product_code][0] = product_price

        else:
            product_name = input('Enter name of product: ')
            print(f'Adding entry {product_code}, {product_price}, {product_name} into products')
            product_dict[product_code] = [product_price, product_name]


if __name__ == '__main__':
    main()