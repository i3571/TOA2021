def runqa1():

    number = int(input('Enter 2-digit number: '))

    is_valid = False

    while (number > 0):
        number, r = divmod(number, 10)

        if r == 1:
            is_valid = True
            break

    print('Valid') if is_valid else print('Invalid')

def runqa2():

    s = input('Enter exactly 3 characters: ')

    if s[0] == s[1] and s[1] == s[2]:
        print('Group A')
    elif s[0] == s[2] and s[0] != s[1]:
        print('Group B')
    elif s[0] != s[2]:
        print('Group C')


def runqa3():

    lst = list(map(int, input('Enter numbers: (a b c d e ...) ').split()))

    total = sum(lst[::2])

    if total % 2 == 0:
        print('The sum of digits in even positions is even.')
    else:
        print('The sum of digits in even positions is odd.')


def runqb1():

    s = input('Enter string that contains sequence of digits: ')

    for ch in reversed(s):
        if ch.isdigit():
            print(int(ch)-1)


def runqb2():

    s = input('Enter string that contains digits (d0d1...dn): ')

    exceed_12 = False

    for i in range(len(s)-1):
        if int(s[i]) + int(s[i+1]) > 12:
            exceed_12 = True
            break

    print(True) if exceed_12 else print(False)


def main():
    question = {'a1': runqa1, 'a2': runqa2, 'a3': runqa3, 'b1': runqb1, 'b2': runqb2}
    opt = input(('Choose question: (a1, a2, a3, b1, b2) '))
    question[opt]()


if __name__ == '__main__':
    main()