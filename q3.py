import random


def roll():
    throw1 = random.randint(1,6)

    while True:
        throw2 = random.randint(1,6)
        if throw2 != throw1:
            break

    while True:
        throw3 = random.randint(1,6)
        if throw3 != throw2 and throw3 != throw1:
            break

    return throw1, throw2, throw3


def playGame(playerList, maxLimit):
    highest = -1
    winners = []

    for player in playerList:
        total = sum(roll())
        # print(f'{player=} {total=}')
        if total > highest and total <= maxLimit:
            winners.clear()
            highest = total
            winners.append(player)
        elif total == highest and total <= maxLimit:
            winners.append(player)

    return winners


def print_menu():
    print('Menu')
    print('1. Change max limit for sum of 3 rolls')
    print('2. Play Game')
    print('0. Exit')


def get_choice():

    while True:
        choice = int(input('Enter choice: '))
        if 0 <= choice <= 2:
            break
        else:
            print('Invalid choice')

    return choice


def change_max_limit():

    while True:
        new_max = int(input('Enter new max limit: '))
        if 10 <= new_max <= 18:
            break
        elif new_max < 10:
            print('Please enter a number that is at least 10')
        elif new_max > 18:
            print('Please enter a number that is at most 18')

    return new_max


def main():
    players = ['A', 'B', 'C', 'D', 'E']
    maxLimit = 12

    while True:
        print_menu()
        opt = get_choice()
        if opt == 1:
            maxLimit = change_max_limit()
        elif opt == 2:
            winners = playGame(players, maxLimit)
            print(', '.join(winners))
        elif opt == 0:
            print('Application ends')
            break


if __name__ == '__main__':
    main()